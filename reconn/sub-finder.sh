#!/bin/bash

url=$1

if [ ! -d  "$url" ]; then
	mkdir $url
fi

if [ ! -d "$url/reconn" ]; then
	#statements
	mkdir $url/reconn
fi



# Using assetfinder to find subdomains
echo "[+] Harvesting subs with assetfinder"
#Harvests all the sub-domains of the given URL (Can give subdomain's which are not under given URL)
assetfinder $url >> $url/reconn/assest.txt 
#Collects subdomains which are only under given URL
cat $url/reconn/assest.txt | grep $1 >> $url/reconn/final.txt
#Removes assests.txt which has all domains(even subs which are out of scope)
rm $url/reconn/assest.txt




# USing amass to find subdomains
#echo "[+] Harvesting subs with amass"
#Gathers everything related to given URL
#amass enum -d $url >> $url/reconn/f.txt
#Sorts everything we find and send it into final.txt
#sort -u $url/reconn/f.txt >> $url/reconn/final.txt
#Removes temp file
#rm $url/reconn/f.txt


#probing for alive domains
echo "[+] Probing for alive domains"
#Finds which subdomains are alive with httprobe
cat $url/reconn/final.txt | httprobe -s -p https:443 | sed 's/https\?:\/\///' | tr -d ':443' >> $url/reconn/alive.txt



#Requirements :

#1. assetfinder
#2. amass
#3. httprobe

#How to use :

#./sub-finder.sh tesla.com
#!/bin/bash

#This script logs the public address of the laptop every single day
ipa=`ipa`
DATE=`date +%d-%m-%Y`
# Change home_path value with your home path
home_path='/home/p/'
file_name='ip'

echo $ipa'---'$DATE >> $home_path$file_name
